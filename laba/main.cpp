#include <string>
#include <iostream>
#include <fstream>
#include "Horse.h"
#include "HorseArray.h"

int main(int argc, char * argv[])
{
	if (argc < 3)
	{
		std::cout << "Wrong format" << std::endl;
		system("pause");
		return -1;
	}
std::ifstream rd(argv[1]);
	if (!rd)
	{
		std::cout << "Error reading file" << std::endl;
		system("pause");
		return -2;
	}
	HorseArray horses;
	rd >> horses;
	rd.close();
	std::cout << horses;
	Horse * forChange = horses ["Zakon"];
	if (forChange)
	{
		Jockey jky1{ "Sam", 17, (charJockey)0 };
		*forChange += jky1;
		++ *forChange;
		(*forChange)[1].ager = 18;
	}
	std::cout << horses;
	std::ofstream wr(argv[2]);
	if (!wr)
	{
		std::cout << "Error writng file" << std::endl;
		system("pause");
		return -3;
	}
	wr << horses;
	horses.GetByGender(wr, woman);
	wr.close();
	system("pause");
}
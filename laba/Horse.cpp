#include <string>
#include <iostream>
#include "Horse.h"

Horse::Horse(std::string nm, std::string co, int a) :
	name(nm), country(co), age(a), jockeys(nullptr), count(0), maxJockeyCount(0)
{
}

Horse::Horse() : jockeys(nullptr), count(0), maxJockeyCount(0)
{
}


void Horse::Print(std::ostream& out) const
{
	if (name != "")
	{
		const char * strJockey[JOCKEY_CHAR] = { "man", "woman" };
		out << "Horse is " << name << std::endl;
		out << "It's age is " << age << std::endl;
		out << "Country is " << country << std::endl;
		out << "Jockeys: " << count << std::endl;
		for (int i = 0; i < count; i++)
		{
			out << "Name: " << jockeys[i].namer << std::endl;
			out << "Age: " << jockeys[i].ager << std::endl;
			out << "Sex: " << strJockey[jockeys[i].chrj] << std::endl;
		}
	}
}

Horse::Horse(std::string nm, std::string co, int a, Jockey * j, int len) :
	name(nm), country(co), age(a), count(len)
{
	maxJockeyCount = len * 2 + 1;
	jockeys = new Jockey[maxJockeyCount];
	for (int i = 0; i < count; i++)
		jockeys[i] = j[i];
}

Horse::Horse(const Horse & ref) : name(ref.name), country(ref.country), age(ref.age), maxJockeyCount(ref.maxJockeyCount), count(ref.count)
{
	jockeys = new Jockey[maxJockeyCount];
	for (int i = 0; i < count; i++)
		jockeys[i] = ref.jockeys[i];
}

Horse::~Horse()
{
	if (jockeys)
		delete[] jockeys;
}

void Horse::AddJockey(std::string namer, int ager, charJockey chrj)
{
	if (count == maxJockeyCount)
	{
		maxJockeyCount *= 2;
		maxJockeyCount++;
		Jockey * temp = new Jockey[maxJockeyCount];
		for (int i = 0; i < count; i++)
			temp[i] = jockeys[i];
		delete[]jockeys;
		jockeys = temp;
	}
	jockeys[count].namer = namer;
	jockeys[count].ager = ager;
	jockeys[count].chrj = chrj;
	count++;
}

void Horse::AddJockey(const Jockey & jky)
{
	AddJockey(jky.namer, jky.ager, jky.chrj);
}

void Horse::RemoveJockey(const Jockey & jky)
{
	RemoveJockey(jky.namer);
}

void Horse::RemoveJockey(std::string namer)
{
	int i = 0;
	for (; i < count; i++)
	{
		if (jockeys[i].namer == namer)
			break;
	}
	if (i != count)
	{
		jockeys[i] = jockeys[count - 1];
		count--;
	}
}

Jockey * Horse::GetJockeys(int & size) const
{
	if (count)
	{
		Jockey *temp = new Jockey[count];
		for (int i = 0; i < count; i++)
			temp[i] = jockeys[i];
		size = count;
		return temp;
	}
	return nullptr;
}

Horse & Horse::operator=(const Horse & ref)
{
	if (this != &ref)
	{
		delete[] jockeys;
		jockeys = new Jockey[maxJockeyCount = ref.maxJockeyCount];
		count = ref.count;
		for (int i = 0; i < count; i++)
		{
			(*this) += ref[i];
		}
		name = ref.name;
		country = ref.country;
		age = ref.age;
	}
	return *this;
}Horse & Horse::operator+=(const Jockey & jky)
{
	AddJockey(jky);
	return *this;
}Horse & Horse::operator-=(const Jockey & jky)
{
	RemoveJockey(jky);
	return *this;
}Jockey Horse::operator[](int index) const
{
	if (count)
		return jockeys[index%count];
}Jockey& Horse::operator[](int index)
{
	if (count)
		return jockeys[index%count];
}Horse & Horse::operator++()
{
	age += 1;
	return *this;
}Horse Horse::operator++(int)
{
	Horse temp(*this);
	++*this;
	return temp;
}Horse & Horse::operator--()
{
	age -= 1;
	return *this;
}Horse Horse::operator--(int)
{
	Horse temp(*this);
	--*this;
	return temp;
}std::ostream & operator<<(std::ostream & out, const Horse & h)
{
	h.Print(out);
	return out;
}

std::istream & operator >> (std::istream & in, Horse & h)
{
	std::string name, country;
	int age;
	in >> name >> country >> age;
	h.SetName(name);
	h.SetAge(age);
	h.SetCountry(country);
	int jockey;
	in >> jockey;
	for (int j = 0; j < jockey; j++)
	{
		std::string namer;
		int ager;
		int chrj;
		in.ignore();
		in >> namer >> ager >> chrj;
		Jockey jky = { namer, ager, (charJockey)chrj };
		h += jky;
	}
	return in;
}
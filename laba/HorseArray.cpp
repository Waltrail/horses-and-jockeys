#include <string>
#include <fstream>
#include "Horse.h"
#include "HorseArray.h"

HorseArray::HorseArray() :maxSize(20), count(0)
{
	arr = new Horse*[maxSize];
	for (int i = 0; i < maxSize; i++)
		arr[i] = nullptr;
}

HorseArray::HorseArray(HorseArray && ref) :arr(std::move(ref.arr)), maxSize(ref.maxSize), count(ref.count)
{
	ref.arr = nullptr;
	maxSize = 0;
	count = 0;
}

HorseArray::~HorseArray()
{
	for (int i = 0; i < maxSize; i++)
		delete arr[i];
	delete[]arr;
}

void HorseArray::Print(std::ostream& out) const
{
	for (int i = 0; i < count; i++)
		arr[i]->Print(out);
}
void HorseArray::GetByGender(std::ostream& out, charJockey gender) const
{
	int cnt = 0;
	for (int i = 0; i < count; i++) {
		for (int j = 0; j < arr[i]->GetJockeysCount(); j++) {
			if (arr[i]->operator[](j).chrj == gender)
				cnt++;			
		}
	}	
	out << std::string("\n���������� ������ " + std::string((gender == 1)? "�������� ": "�������� ") +"����: " + std::to_string(cnt));
}
void HorseArray::Read(std::istream & in)
{
	int temp;
	in >> temp;
	for (int i = 0; i < temp; i++)
	{
		in.ignore();
		std::string nm;
		std::string co;
		int a;
		in >> nm >> co >> a;
		AddHorse(nm, co, a);
		int jockey;
		in >> jockey;
		for (int j = 0; j < jockey; j++)
		{
			std::string namer;
			int ager, chrj;
			in.ignore();
			in >> namer >> ager >> chrj;
			arr[count - 1]->AddJockey(namer, ager, (charJockey)chrj);
		}
	}
}

void HorseArray::AddHorse(std::string nm, std::string co, int a)
{
	if (maxSize == count)
	{
		Horse ** temp = new Horse*[maxSize = maxSize * 2 + 1];
		for (int i = 0; i < count; i++)
		{
			temp[i] = arr[i];
		}
		delete[]arr;
		arr = temp;
	}
	arr[count++] = new Horse(nm, co, a);
}


bool HorseArray::RemoveHorse(std::string nm)
{
	int i = 0;
	for (; i < count; i++)
	{
		if (arr[i]->GetName() == nm)
		{
			arr[i] = arr[count - 1];
			return true;
		}
	}
	if (i == count)
		return false;
}

Horse * HorseArray::GetHorseByNumber(int index)
{
	if (count < index && index >= 0)
		return arr[index];
	return nullptr;
}

Horse * HorseArray::GetHorseByName(std::string nm)
{
	for (int i = 0; i < count; i++)
	{
		if (arr[i]->GetName() == nm)
			return arr[i];
	}
	return nullptr;
}

HorseArray & HorseArray::operator+=(std::string nm)
{
	AddHorse(nm, nullptr, 0);
	return *this;
}HorseArray & HorseArray::operator-=(std::string nm)
{
	RemoveHorse(nm);
	return *this;
}Horse * HorseArray::operator[](int index)
{
	return GetHorseByNumber(index);
}Horse * HorseArray::operator[](std::string nm)
{
	return GetHorseByName(nm);
}std::ostream & operator<<(std::ostream & out, const HorseArray & ha)
{
	ha.Print(out);
	return out;
}std::istream & operator >> (std::istream & in, HorseArray & ha)
{
	ha.Read(in);
	return in;
}
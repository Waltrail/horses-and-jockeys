#ifndef HORSEARRAY_H
#define HORSEARRAY_H
#include <string>
#include <iostream>

class HorseArray
{
public:
	HorseArray();
	HorseArray(HorseArray && ref);
	~HorseArray();
	void Print(std::ostream& out)const;
	void GetByGender(std::ostream & out, charJockey gender) const;
	void Read(std::istream& in);
	void AddHorse(std::string nm, std::string �o, int a);
	bool RemoveHorse(std::string nm);
	Horse* GetHorseByNumber(int index);
	Horse* GetHorseByName(std::string nm);
	int Count()const { return count; }
	HorseArray& operator+=(std::string nm);
	HorseArray& operator-=(std::string nm);
	Horse* operator[](int index);
	Horse* operator[](std::string nm);
private:
	HorseArray(const Horse& ref) {}
	Horse ** arr;
	int count;
	int maxSize;
	HorseArray& operator=(const Horse& ref) {}
};

std::ostream& operator<<(std::ostream& out, const HorseArray & ha);
std::istream& operator >> (std::istream& in, HorseArray & ha);

#endif

#ifndef HORSE_H
#define HORSE_H
#include <string>
#include <iostream>

#define JOCKEY_CHAR 2
enum charJockey { man, woman };

struct Jockey
{
	std::string namer;
	int ager;
	charJockey chrj;
};

class Horse
{
public:
	Horse();
	Horse(std::string nm, std::string co, int a);
	std::string GetName()const { return name; }
	void SetName(std::string nm) { name = nm; }
	std::string GetCountry()const { return country; }
	void SetCountry(std::string co) { country = co; }
	int GetAge()const { return age; }
	int GetJockeysCount()const { return count; }
	void SetAge(int a) { age = a; }
	void Print()const;
	Horse(std::string nm, std::string co, int a, Jockey * j, int len);
	Horse(const Horse & ref);
	~Horse();
	void AddJockey(std::string namer, int ager, charJockey chrj);
	void AddJockey(const Jockey & jky);
	void RemoveJockey(const Jockey & jky);
	void RemoveJockey(std::string namer);
	Jockey * GetJockeys(int & size)const;
	void Print(std::ostream& out)const;
	Horse& operator=(const Horse& ref);
	Horse& operator+=(const Jockey& jky);
	Horse& operator-=(const Jockey& jky);
	Jockey operator[](int index)const;
	Jockey& operator[](int index);
	Horse& operator++();
	Horse operator++(int);
	Horse& operator--();
	Horse operator--(int);
private:
	std::string name, country;
	int age;
	Jockey * jockeys;
	int maxJockeyCount;
	int count;
};

std::ostream& operator<<(std::ostream& out, const Horse& h);
std::istream& operator >> (std::istream& in, Horse& h);
#endif
	